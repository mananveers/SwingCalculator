# SwingCalculator

Solutions for the assignments given on Day 6.

Contains three packages package:

- `arrayListAssn`
- `generics`
- `swing`

`arrayListAssn` contains a single class, `Menu`, that has a `main()` method. `generics` contains a base class `GenericArray` and a companion `Tester` class that has a `main()` method. `swing` contains one class `Calculator` that comes with a `main()` method.